package VendaEntrades;

public class VendaEntrades {

    public static void main(String[] args) {
        Taquilla persona1 = new Taquilla();
        Taquilla persona2 = new Taquilla();
        Taquilla persona3 = new Taquilla();
        Taquilla persona4 = new Taquilla();
        Taquilla persona5 = new Taquilla();

        persona1.reservarEntrades("Carlos", 3);
        persona2.reservarEntrades("Juan", 5);
        persona3.reservarEntrades("Franziska", 2);
        persona4.reservarEntrades("Leopoldo", 3);
        persona5.reservarEntrades("Ariel", 1);

        persona1.start();
        persona2.start();
        persona3.start();
        persona4.start();
        persona5.start();
    }
}
