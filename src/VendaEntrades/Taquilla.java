package VendaEntrades;

public class Taquilla extends Thread{
    private String usuari;
    private int entrades;
    private static int maxEntrades = 10;

    synchronized void reservarEntrades (String nomUsuari, int numEntrades){
        usuari = nomUsuari;
        entrades = numEntrades;
        maxEntrades -= numEntrades;
    }

    @Override
    public void run() {
        reservarEntrades(usuari, entrades);
        if (entrades > 4){
            System.out.println("No es poden comprar més de 4 entrades, "+usuari);
            this.interrupt();
        } else if (maxEntrades <= 0){
            System.out.println("No queden entrades, "+usuari);
        } else {
            System.out.println(usuari + ", entrades comprades satisfactoriament.");
        }
    }
}
