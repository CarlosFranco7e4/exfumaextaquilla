package DeixarDeFumar;

public class DeixarDeFumar {
    private static Thread t1;

    public static void main(String[] args) {
        Runnable r1=()->fumador();
        Runnable r2=()->metge();
        t1=new Thread(r1);
        Thread t2 = new Thread(r2);
        t1.start();
        t2.start();
    }

    public static void fumador(){
        int i = 0;
        while (!t1.isInterrupted()) {
            System.out.println("Fumo cigarreta... "+ i);
            i++;
        }

    }

    public static void metge(){
        try {
            Thread.sleep(3000);
            t1.interrupt();
            System.out.println("Deixa de fumar!");
        } catch (InterruptedException e) {
            System.out.println(e);   }
    }
}
